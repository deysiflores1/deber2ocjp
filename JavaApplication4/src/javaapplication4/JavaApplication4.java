/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication4;

/**
 *
 * @author USRKAP
 */
public class JavaApplication4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
         System.out.println("Escuela Politecnica Nacional");
        System.out.println("Deber de Oracle Certified Java Programmer");
        System.out.println("Autor:Deysi Tacán");
        
        Persona p1 = new Persona();
        Persona p2 = new Persona("Deysi");
        Persona p3 = new Persona(23);
        Persona p4 = new Persona("Deysi", 23);
        System.out.println("Uso de Sobrecarga de metodos");
        System.out.println("Nombre Persona1: " + p1.nombre + ", edad persona1: " + p1.edad);
        System.out.println("Nombre persona 2: " + p2.nombre + ", edad persona2: " + p2.edad);
        System.out.println("Nombre persona3: " + p3.nombre + ", edad persona3: " + p3.edad);
        System.out.println("Nombre persona4: " + p4.nombre + ", edad persona4: " + p4.edad);
        
        
         System.out.println("Uso de interfaces");
        
          Persona p = new Persona();
          hacerCaminar((Relacion) p);
          
          System.out.println("Uso de Enunms");
        EnumsUse tiposdec;
        tiposdec = EnumsUse.FORD;
        System.out.println ("El auto escogi es " + tiposdec.toString().toLowerCase() );
        System.out.println ("El auto escogido es: FORD: Resultado " + (tiposdec==EnumsUse.FORD) );
        System.out.println ("Es el auto escogido: FORD Resultado: " + (tiposdec==EnumsUse.TOYOTA) );
         System.out.println("Uso de relacion");
           Profesor profesor1 = new Profesor ("Salome", "Flores", 25);
          profesor1.setIdProfesor("0175698745");
          profesor1.mostrarNombreApellidosYCarnet();
          
    }
    public static void hacerCaminar(Relacion c)
	            {
		            c.caminar();
	            }
}
